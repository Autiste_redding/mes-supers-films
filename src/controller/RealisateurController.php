<?php

class RealisateurController
{

    public $conn;

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function getRealById($id)
    {
        $query = "
            SELECT 
                *
            FROM
                realisateurs
            WHERE
                id = :id
            ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute([':id' => $id]);

        if ($stmt->rowCount() > 0) {
            $res = $stmt->fetchAll()[0];

            $real = new Realisateur($res['name']);
        } else {
            $res = null;
        }

        return $real;
    }
}
